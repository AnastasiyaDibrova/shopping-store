from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted((item, item) for item in get_all_styles())


class Product(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(blank=False, default='')
    price = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    creator = models.ForeignKey(
        'auth.User', related_name='products', on_delete=models.CASCADE, null=True)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created_date', )
