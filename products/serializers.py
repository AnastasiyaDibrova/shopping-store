from django.contrib.auth.models import User
from rest_framework import serializers

from products.models import Product


class UserSerializer(serializers.HyperlinkedModelSerializer):
    snippets = serializers.HyperlinkedRelatedField(
        many=True, view_name='snippet-detail', read_only=True)

    products = serializers.HyperlinkedRelatedField(
        many=True, view_name='product-detail', read_only=True)

    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'products', 'products')


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    creator = serializers.ReadOnlyField(source='creator.username')

    class Meta:
        model = Product
        fields = ('id', 'title', 'description', 'price', 'creator', 'created_date')
