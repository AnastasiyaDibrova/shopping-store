from django.contrib.auth.models import User
from rest_framework import permissions, viewsets

from products.models import Product
from products.serializers import UserSerializer, ProductSerializer


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        # IsCreatorOrReadOnly,
    )

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)
